<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */


/**
 * Seems that there is an issue in WP when setting a featured image for CPT:
 * attachment is uploaded and bound to the post but not displayed as its featured image.
 * Therefore, a workaround is added to the template - which is questionable but working for now.
 */
global $wpdb;
$attachment = $wpdb->get_row( $wpdb->prepare( 'SELECT * FROM wp_posts WHERE post_parent=%s', get_the_ID() ) );
$post_thumbnail = has_post_thumbnail() ? get_the_post_thumbnail_url() : $attachment->guid;

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
  <a href="<?php the_permalink(); ?>">
    <?php the_title( '<h2>', '</h2>' ); ?>
  </a>

  <?php if ( $post_thumbnail ) : ?>
    <figure class="featured-media">
      <div class="featured-media-inner section-inner">
        <img src="<?php echo $post_thumbnail; ?>" alt="Featured image">
      </div>
    </figure>
  <?php endif; ?>

  <div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">

    <div class="entry-content">
      <div class="entry-meta">
        <p>
          <?php
          _e( 'Vacancy ID: ', 'twentytwentychild' );
          the_field('vacancy_id', get_the_ID());
          ?>
        </p>
        <p><strong><?php the_field( 'alternative_company_name', get_the_ID() ); ?></strong></p>
        <div class="flex-container">
          <div class="col-1-2"><?php
            _e( 'Vacancy start date: ', 'twentytwentychild' );
            echo date( 'j F Y', get_field( 'start_date', get_the_ID() ) );
            //          the_field( 'start_date', get_the_ID() );
            ?></div>
          <div class="col-1-2"><?php
            _e( 'Vacancy end date: ', 'twentytwentychild' );
            echo date( 'j F Y', get_field( 'end_date', get_the_ID() ) );
            //          the_field( 'start_date', get_the_ID() );
            ?></div>
        </div>
        <p>
          <?php
          _e( 'Location: ', 'twentytwentychild' );
          the_field( 'location', get_the_ID() );
          ?>
        </p>
      </div>

      <?php
      if ( is_search() || is_archive() || ! is_singular() && 'summary' === get_theme_mod( 'blog_content', 'full' ) ) {
        the_excerpt();
      } else {
        the_content( __( 'Continue reading', 'twentytwentychild' ) );
      }
      ?>

    </div><!-- .entry-content -->

  </div><!-- .post-inner -->

  <div class="section-inner">
    <?php
    wp_link_pages(
      array(
        'before'      => '<nav class="post-nav-links bg-light-background" aria-label="' . esc_attr__( 'Page', 'twentytwenty' ) . '"><span class="label">' . __( 'Pages:', 'twentytwenty' ) . '</span>',
        'after'       => '</nav>',
        'link_before' => '<span class="page-number">',
        'link_after'  => '</span>',
      )
    );

    edit_post_link();

    // Single bottom post meta.
    twentytwenty_the_post_meta( get_the_ID(), 'single-bottom' );

    if ( is_single() ) {

      get_template_part( 'template-parts/entry-author-bio' );

    }
    ?>

  </div><!-- .section-inner -->

  <?php

  if ( is_single() ) {

    get_template_part( 'template-parts/navigation' );

  }

  /**
   *  Output comments wrapper if it's a post, or if comments are open,
   * or if there's a comment number – and check for password.
   * */
  if ( ( is_single() || is_page() ) && ( comments_open() || get_comments_number() ) && ! post_password_required() ) {
    ?>

    <div class="comments-wrapper section-inner">

      <?php comments_template(); ?>

    </div><!-- .comments-wrapper -->

    <?php
  }
  ?>

</article><!-- .post -->
