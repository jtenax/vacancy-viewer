<?php

require_once 'vacancy-acf.php';

add_action( 'wp_enqueue_scripts', 'load_theme_assets' );
function load_theme_assets() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css' );
}

if ( get_page_by_path( 'vacancy' ) == NULL ) {
  $args = array(
    'post_title'  => 'Vacancies Archive',
    'post_type'   => 'page',
    'post_name'   => 'vacancy',
    'post_status' => 'publish'
  );
  wp_insert_post( $args );
}

add_action( 'init', 'register_post_types' );
function register_post_types() {
	$labels = array(
		'name'                  => _x( 'Vacancies', 'Post type general name', 'twentytwentychild' ),
		'singular_name'         => _x( 'Vacancy', 'Post type singular name', 'twentytwentychild' ),
		'menu_name'             => _x( 'Vacancies', 'Admin Menu text', 'twentytwentychild' ),
		'name_admin_bar'        => _x( 'Vacancy', 'Add New on Toolbar', 'twentytwentychild' ),
		'add_new'               => __( 'Add New', 'twentytwentychild' ),
		'add_new_item'          => __( 'Add New Vacancy', 'twentytwentychild' ),
		'new_item'              => __( 'New Vacancy', 'twentytwentychild' ),
		'edit_item'             => __( 'Edit Vacancy', 'twentytwentychild' ),
		'view_item'             => __( 'View Vacancy', 'twentytwentychild' ),
		'all_items'             => __( 'All Vacancies', 'twentytwentychild' ),
		'search_items'          => __( 'Search Vacancies', 'twentytwentychild' ),
		'parent_item_colon'     => __( 'Parent Vacancies:', 'twentytwentychild' ),
		'not_found'             => __( 'No vacancies found.', 'twentytwentychild' ),
		'not_found_in_trash'    => __( 'No vacancies found in Trash.', 'twentytwentychild' ),
		'featured_image'        => __( 'Vacancy Image', 'twentytwentychild' ),
		'set_featured_image'    => __( 'Set vacancy image', 'twentytwentychild' ),
		'remove_featured_image' => __( 'Remove vacancy image',  'twentytwentychild' ),
		'use_featured_image'    => __( 'Use as vacancy image', 'twentytwentychild' ),
		'archives'              => __( 'Vacancy archives', 'twentytwentychild' ),
		'insert_into_item'      => __( 'Insert into vacancy', 'twentytwentychild' ),
		'uploaded_to_this_item' => __( 'Uploaded to this vacancy', 'twentytwentychild' ),
		'filter_items_list'     => __( 'Filter vacancies list', 'twentytwentychild' ),
		'items_list_navigation' => __( 'Vacancies list navigation', 'twentytwentychild' ),
		'items_list'            => __( 'Vacancies list', 'twentytwentychild' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'vacancy' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
	);

	register_post_type( 'vacancy', $args );
}


class Data_Importer {

  private $address = 'https://onsweb.nl/wp-content/uploads/backend-senior-case-1';
  private $extension = '.xml';

  private function get_response_arr( $filename ) {
    $response = wp_remote_get( trailingslashit( $this->address ) . $filename . $this->extension );

    $xml_string = htmlspecialchars_decode( $response['body'] );
    $xml_string = preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $xml_string);
    $xml_string = preg_replace( '/&(?!#?[a-z0-9]+;)/', '&amp;', $xml_string );
    $xml_string = simplexml_load_string( $xml_string );

    return $xml_string;
  }

  function get_ids_array() {
    $all_vacancies = self::get_response_arr( 'list' );
    $vacancies_ids_arr = [];

    foreach ( $all_vacancies->children() as $vacancy ) {
      $vacancies_ids_arr[] = (int) $vacancy->attributes()->id;
    }

    return $vacancies_ids_arr;
  }

  function get_existing_ids_array() {
    $existing_ids_arr = [];
    $args = array(
      'post_type'      => 'vacancy',
      'numberposts' => -1,
    );
    $vacancies = get_posts( $args );
    foreach ( $vacancies as $vacancy ) {
      $existing_ids_arr[] = $vacancy->ID;
    }
    return $existing_ids_arr;
  }

  function delete_old_vacancies() {
    global $wpdb;

    $actual_vacancies = $this->get_ids_array();
    $existing_vacancies = $this->get_existing_ids_array();

    foreach ( $existing_vacancies as $existing_vacancy ) {
      if ( !in_array( get_field( 'vacancy_id', $existing_vacancy ), $actual_vacancies ) ) {

        if ( has_post_thumbnail( $existing_vacancy ) ) {
          $attachment_id = get_post_thumbnail_id( $existing_vacancy );
        } else {
          $attachment_id = $wpdb->get_var( $wpdb->prepare( 'SELECT id FROM wp_posts WHERE post_parent=%s', $existing_vacancy ) );
        }
//          $wpdb->delete( 'wp_posts', array( 'post_parent' => $existing_vacancy ) );
        wp_delete_attachment( $attachment_id );
        wp_delete_post( $existing_vacancy );
      }
    }
  }

  /**
   * Not sure what conditions should be checked for update,
   * so only two cases are implemented for demonstration.
   */
  function update_vacancies() {
    $existing_vacancies = $this->get_existing_ids_array();
    foreach ( $existing_vacancies as $existing_vacancy ) {
      $vacancy = get_post( $existing_vacancy );
      $post_data = self::get_response_arr( get_field( 'vacancy_id', $existing_vacancy ) );
      if ( empty( $vacancy->post_content ) ) {
        $args = array(
          'ID' => $existing_vacancy,
          'post_content' => $post_data->Versions->Version->Description->asXML(),
        );
        wp_update_post( $args );
      }
      if ( !has_post_thumbnail( $vacancy ) ) {
        $image_url = (string) $post_data->Departments->Department->ImageURL;
        $this->create_attachment_from_url( $image_url, $vacancy->ID );
      }
    }
  }

  function create_single_post( $attributes, $post_data ) {
    $args = array(
      'post_status'  => 'publish',
      'post_type'    => 'vacancy',
      'post_title'   => $post_data->Versions->Version->Title,
      'post_excerpt' => $post_data->Versions->Version->TitleHeading,
      'post_content' => $post_data->Versions->Version->Description->asXML(),
      'meta_input'   => array(
        'vacancy_id' => (int) $attributes['id'],
        'start_date' => strtotime( $attributes['date_start'] ),
        'end_date'   => strtotime( $attributes['date_end'] ),
        'alternative_company_name' => (string) $post_data->Versions->Version->AlternativeCompanyName,
        'location' => (string) $post_data->Versions->Version->Location,
      )
    );
    $image_url = (string) $post_data->Departments->Department->ImageURL;
    $post_id = wp_insert_post( $args );
    $attachment_id = $this->create_attachment_from_url( $image_url, $post_id );
  }

  /**
   * Seems that there is an issue in WP when setting a featured image for CPT:
   * attachment is uploaded and bound to the post but not displayed as its featured image.
   * Therefore, a workaround is added to the template - which is questionable but working for now.
   */
  public function create_attachment_from_url( $url, $parent_post_id ) {
    $response = wp_remote_get( $url );

    $uploading_file = wp_upload_bits( basename( $url ), null, $response['body'] );
    if ( !empty( $uploading_file['error'] ) ) {
      return false;
    }

    $file_path = $uploading_file['file'];
    $file_name = basename( $file_path );
    $file_type = wp_check_filetype( $file_name, null );
    $attachment_title = sanitize_file_name( pathinfo( $file_name, PATHINFO_FILENAME ) );
    $wp_upload_dir = wp_upload_dir();

    $attachment_post_data = array(
      'guid' => trailingslashit( $wp_upload_dir['url'] ) . $file_name,
      'post_mime_type' => $file_type['type'],
      'post_title' => $attachment_title,
      'post_content' => '',
      'post_status' => 'inherit',
    );

    $attachment_id = wp_insert_attachment( $attachment_post_data, $file_path, $parent_post_id );

    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    $attachment_data = wp_generate_attachment_metadata( $attachment_id, $file_path );
    wp_update_attachment_metadata( $attachment_id, $attachment_data );
    return $attachment_id;
  }

  function create_posts() {
    $existing_vacancies = $this->get_existing_ids_array();
    $existing_vacancies_ids = [];
    foreach ( $existing_vacancies as $existing_vacancy ) {
      $existing_vacancies_ids[] = get_field( 'vacancy_id', $existing_vacancy );
    }

    foreach ( $this->get_ids_array() as $vacancy_id ) {
      if ( in_array( $vacancy_id, $existing_vacancies_ids ) ) {
        continue;
      } else {
        $attributes = self::get_response_arr( $vacancy_id )->attributes();
        $post_data = self::get_response_arr( $vacancy_id );

        $this->create_single_post( $attributes, $post_data );
      }
    }
  }


  function create_or_update_vacancies() {
    $this->delete_old_vacancies();
    $this->create_posts();
    $this->update_vacancies();
  }

}

  // Vacancy creation/update to be fired by Cron
  add_action( 'wp', 'vacancy_updater' );
  function vacancy_updater() {
    if( ! wp_next_scheduled( 'daily_vacancy_update' ) ) {
      wp_schedule_event( time(), 'daily', 'daily_vacancy_update');
    }
  }

  add_action( 'daily_vacancy_update', 'daily_vacancy_update' );
  function daily_vacancy_update() {
    $data_importer = new Data_Importer();
    $data_importer->create_or_update_vacancies();
  }
