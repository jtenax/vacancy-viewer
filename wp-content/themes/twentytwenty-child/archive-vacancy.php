<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
$vacancy_archive_id = get_page_by_path( 'vacancy' )->ID;
?>

<main id="site-content" role="main">
  <?php
    if ( is_archive() ) {
      $page_title = get_the_title( $vacancy_archive_id );
    }
  ?>

  <?php if ( $page_title ) : ?>
    <h1><?php echo $page_title; ?></h1>
  <?php endif; ?>
  <?php

  //  $data_importer = new Data_Importer();
  //  $data_importer->output_result();
  ?>
  <?php
  if ( have_posts() ) {

    while ( have_posts() ) {
      the_post();

      get_template_part( 'template-parts/content', get_post_type() );
    }
  }

  the_posts_pagination();
  ?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
