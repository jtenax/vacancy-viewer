For a real-life case, we'd probably create a more general solution able to import from an arbitrary XML, not just the certain one. Also, the class design would be thought-out in greater details and contain additional validity and/or existence checks. The present solution is just a quick example.

Seems that there is an issue in WP when setting a featured image for CPT: attachment is uploaded and bound to the post but not displayed as its featured image. Therefore, a workaround is added to the template - which is questionable but working for now.

Vacancy import is intended to be fired with WP Crontrol plugin. To start importing:
* Please visit the plugin manage page /wp-admin/tools.php?page=crontrol_admin_manage_page
* Click 'Run Now' link under 'daily_vacancy_update' hook.

It may take several minutes to import data. Please wait until all data are imported.
Please make sure that the uploads folder is writable.
